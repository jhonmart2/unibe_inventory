-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-08-2017 a las 06:35:12
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `unibe_inventory`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(255) NOT NULL,
  `descripcion_categoria` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `nombre_categoria`, `descripcion_categoria`, `date_added`) VALUES
(1, 'ComunicaciÃ³n', 'Libros del Grado de ComunicaciÃ³n.', '2016-12-19 00:00:00'),
(4, 'IngenierÃ­a en TecnologÃ­a de la InformaciÃ³n y ComunicaciÃ³n', 'Libros del Grado de IngenierÃ­a en TecnologÃ­a de la InformaciÃ³n y ComunicaciÃ³n.', '2016-12-19 21:06:37'),
(5, 'Arquitectura', 'Libros del Grado de Arquitectura.', '2016-12-19 21:06:39'),
(10, 'DiseÃ±o de Interiores', 'Libros del Grado de DiseÃ±os de Interiores.', '2017-08-30 05:10:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE `historial` (
  `id_historial` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `nota` varchar(255) NOT NULL,
  `referencia` varchar(100) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historial`
--

INSERT INTO `historial` (`id_historial`, `id_producto`, `user_id`, `fecha`, `nota`, `referencia`, `cantidad`) VALUES
(1, 1, 1, '2017-08-30 05:20:03', 'Jhonatan agregÃ³ 100 producto(s) al inventario', '039485', 100),
(2, 2, 1, '2017-08-30 05:21:46', 'Jhonatan agregÃ³ 50 producto(s) al inventario', '034980', 50),
(3, 3, 1, '2017-08-30 05:23:36', 'Jhonatan agregÃ³ 75 producto(s) al inventario', '098500', 75),
(4, 4, 1, '2017-08-30 05:25:19', 'Jhonatan agregÃ³ 350 producto(s) al inventario', '30495', 350),
(5, 5, 1, '2017-08-30 05:26:46', 'Jhonatan agregÃ³ 215 producto(s) al inventario', '320957', 215),
(6, 6, 1, '2017-08-30 05:27:33', 'Jhonatan agregÃ³ 765 producto(s) al inventario', '0349788', 765),
(7, 7, 1, '2017-08-30 05:28:39', 'Jhonatan agregÃ³ 211 producto(s) al inventario', '40398', 211),
(8, 8, 1, '2017-08-30 05:29:17', 'Jhonatan agregÃ³ 56 producto(s) al inventario', '77657', 56),
(9, 9, 1, '2017-08-30 05:29:53', 'Jhonatan agregÃ³ 143 producto(s) al inventario', '98755', 143),
(10, 10, 1, '2017-08-30 05:39:34', 'Jhonatan agregÃ³ 898 producto(s) al inventario', '39487001', 898),
(11, 11, 1, '2017-08-30 05:40:56', 'Jhonatan agregÃ³ 1234 producto(s) al inventario', '23659997', 1234),
(12, 12, 1, '2017-08-30 05:42:25', 'Jhonatan agregÃ³ 122 producto(s) al inventario', '398780', 122);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id_producto` int(11) NOT NULL,
  `codigo_producto` char(20) NOT NULL,
  `nombre_producto` char(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `precio_producto` double NOT NULL,
  `stock` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id_producto`, `codigo_producto`, `nombre_producto`, `date_added`, `precio_producto`, `stock`, `id_categoria`) VALUES
(1, '039485', 'Fundamentos de Arquitectura', '2017-08-30 05:20:03', 1970.98, 100, 5),
(2, '034980', 'Historia de la Arquitectura', '2017-08-30 05:21:46', 1200.87, 50, 5),
(3, '098500', 'AnÃ¡lisis de la Arquitectura Moderna', '2017-08-30 05:23:36', 1600, 75, 5),
(4, '30495', 'ComunicaciÃ³n ContemporÃ¡nea', '2017-08-30 05:25:19', 789, 350, 1),
(5, '320957', 'IntroducciÃ³n al Desarrollo Comunicativo PsicolÃ³gico', '2017-08-30 05:26:46', 2134.87, 215, 1),
(6, '0349788', 'ComunicaciÃ³n Para Empresas', '2017-08-30 05:27:33', 6457.98, 765, 1),
(7, '40398', 'Fundamentos en Arte Mobiliario', '2017-08-30 05:28:39', 1459, 211, 10),
(8, '77657', 'Fundamentos de DiseÃ±o de Interiores', '2017-08-30 05:29:17', 1677.98, 56, 10),
(9, '98755', 'GuÃ­a Avanzada de DiseÃ±o de Interiores', '2017-08-30 05:29:53', 1238.76, 143, 10),
(10, '39487001', 'GuÃ­a Introductoria a la TecnologÃ­a de la InformaciÃ³n', '2017-08-30 05:39:34', 3445, 898, 4),
(11, '23659997', 'GuÃ­a PrÃ¡ctica de PHP Laravel', '2017-08-30 05:40:56', 4567, 1234, 4),
(12, '398780', 'AdministraciÃ³n de Servidores', '2017-08-30 05:42:25', 8787.99, 122, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL COMMENT 'auto incrementing user_id of each user, unique index',
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data';

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `user_name`, `user_password_hash`, `user_email`, `date_added`) VALUES
(1, 'Jhonatan', 'Martinez', 'admin', '$2y$10$MPVHzZ2ZPOWmtUUGCq3RXu31OTB.jo7M9LZ7PmPQYmgETSNn19ejO', 'admin@admin.com', '2016-12-19 15:06:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `historial`
--
ALTER TABLE `historial`
  ADD PRIMARY KEY (`id_historial`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_producto`),
  ADD UNIQUE KEY `codigo_producto` (`codigo_producto`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `historial`
--
ALTER TABLE `historial`
  MODIFY `id_historial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index', AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `historial`
--
ALTER TABLE `historial`
  ADD CONSTRAINT `fk_id_producto` FOREIGN KEY (`id_producto`) REFERENCES `products` (`id_producto`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
